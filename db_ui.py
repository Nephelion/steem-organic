from db import insert, select_all, delete_all, delete, update, is_in_blacklist

# If you want to fetch all the accounts within the blacklist, run 'select_all()'
# select_all()

# If you want to insert a new account into the blacklist, run 'insert(acc_name)'
# insert(acc_name)

# If you want to insert a list of accounts into the blacklist, run 'update(acc_list)'
# update(acc_list)

# If you want to check whether an account is in the blacklist or not, run 'is_in_blacklist(acc_name)'
# is_in_blacklist(acc_name)

# If you want to delete an account from blacklist, run 'delete(acc_name)'; else if you want to delete all accounts from blacklist, run 'delete_all()'
# delete(acc_name)
# delete_all()
# print(select_all())



