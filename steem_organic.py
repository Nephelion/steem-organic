from steem import Steem
from datetime import time, datetime
from format_functions import remove_non_ascii, unix_time, get_age
from format_functions import smart_format, format_date, italic, concatenate, c, right, l, h, make_list, unix_time, blank_space, remove_non_ascii, str_to_time
from threading import Timer
from s_keys import account_name, private_posting_key, private_active_key
from datetime import datetime
from db import select_all

import json
import math

# Main classes
s  = Steem(keys = [private_posting_key, private_active_key])
bl = select_all()

# Main functions
def submit_post(title, tags, body, author): # Submits a post to Steemit

        s.commit.post(title = title, body = body, author = author, tags = tags)
        print('Post successfully submitted')

def get_posts(number_of_posts):

    if number_of_posts <= 100:
        query1 = {'limit' : number_of_posts}
        posts1 = s.get_discussions_by_trending(query1)
        posts  = posts1
    
    elif number_of_posts > 100 and number_of_posts <= 200:
        query1 = {'limit' : 100}
        posts1 = s.get_discussions_by_trending(query1)
        query2 = {
                'limit' : number_of_posts - 100,
                'start_author' : posts1[-1]['author'],
                'start_permlink' : posts1[-1]['permlink']
                    }
        posts2 = s.get_discussions_by_trending(query2)
        _posts = posts1 + posts2
        posts  = [i for n, i in enumerate(_posts) if i not in _posts[n + 1:]] 

    elif number_of_posts > 200 and number_of_posts <= 300:
        query1 = {'limit' : 100}
        posts1 = s.get_discussions_by_trending(query1)
        query2 = {
                'limit' : 100,
                'start_author' : posts1[-1]['author'],
                'start_permlink' : posts1[-1]['permlink']
                    }
        posts2 = s.get_discussions_by_trending(query2)
        query3 = {
                'limit' : number_of_posts - 200,
                'start_author' : posts2[-1]['author'],
                'start_permlink' : posts2[-1]['permlink']
            }
        posts3 = s.get_discussions_by_trending(query3)
        _posts = posts1 + posts2 + posts3
        posts  = [i for n, i in enumerate(_posts) if i not in _posts[n + 1:]] 

    else:
        print('The maximum number of posts allowed is 300')

    return posts

def get_data(number_of_posts): # Fetches the data for a given number of posts and outputs a dictionary

    posts     = get_posts(number_of_posts)
    data_dict = []

    for i in range(0, len(posts)):
        author   = remove_non_ascii(posts[i]['author'])
        permlink = remove_non_ascii(posts[i]['permlink'])
        title    = remove_non_ascii(posts[i]['title'])
        votes    = posts[i]['active_votes']
        url      = posts[i]['url']
        metadata = json.loads(posts[i]['json_metadata'])
        age      = get_age(unix_time(posts[i]['created']))
        date     = str_to_time(posts[i]['created'])

        if 'image' in metadata.keys():
            if len(metadata['image']) >= 1:
                image = metadata['image'][0]
            else:
                image = '![No_ImageFound.png](https://cdn.steemitimages.com/DQmNsSu6oWxCcJYrNiL8pzjv682iT67MENbPhmiGcAxxJDW/No_ImageFound.png)'
        else: 
                image = '![No_ImageFound.png](https://cdn.steemitimages.com/DQmNsSu6oWxCcJYrNiL8pzjv682iT67MENbPhmiGcAxxJDW/No_ImageFound.png)'

        data_dict.append({'author' : author, 'permlink' : permlink, 'title' : title,
                        'votes' : votes, 'url' : url, 'image' : image, 'age' : age, 'date' : date})

    return data_dict

# Functions
def total_rshares(post_votes = []): # Sums the rshares of all votes and outputs the total rshares for a post

    rshares = []

    for j in range(0, len(post_votes)):
        rshares_ = float(post_votes[j]['rshares'])
        rshares.append(rshares_)

    return sum(rshares)

def get_real_value(age, organic_shares, votes = []): # Provides a trending value to sort the posts before displaying them
    
    t = (age * 1000) / 480000000
    
    if organic_shares <= 0: 
        real_value = 1
    else:
        real_value = get_upvote_value(math.pow(10, math.log10(organic_shares) - t))
    
    return real_value

def get_upvote_value(rshares): # Returns the payout given by rshares

    reward_fund      = s.get_reward_fund('post')
    recent_claims    = float(reward_fund['recent_claims'])
    reward_balance   = reward_fund['reward_balance'].split(' ')
    reward_balance   = float(reward_balance[0])
    sbd_median_price = s.get_current_median_history_price()['base'].split(' ')
    sbd_median_price = float(sbd_median_price[0])
    upvote_value     = round(float((rshares) * (reward_balance / recent_claims)) * sbd_median_price, 4)

    return upvote_value

def payout(top_posts, number_of_posts, data_dict): # Returns a dictionary with payout-related values

    organic_votes = []
    bot_votes     = []
    l             = []
    votes         = data_dict[top_posts]['votes']
    age           = data_dict[top_posts]['age']

    for i in range(0, len(votes)):
        voter = votes[i]['voter']
        if (voter,) in bl: bot_votes.append(votes[i])
        else: organic_votes.append(votes[i])

    organic_shares  = total_rshares(organic_votes)
    organic_payout  = get_upvote_value(organic_shares)
    bot_shares      = total_rshares(bot_votes)
    bot_payout      = get_upvote_value(bot_shares)
    total_payout    = bot_payout + organic_payout
    payout_dict     = {'post_title' : remove_non_ascii(data_dict[top_posts]['title']),
                        'total_payout' : round(total_payout, 3), 'organic_payout' : organic_payout,
                        'relative_organic_payout' : round(organic_payout / total_payout, 3),
                        'bot_payout' : bot_payout, 'relative_bot_payout' : round(bot_payout / total_payout, 3),
                        'organic_trending' : get_real_value(age, organic_shares, votes)}

    return payout_dict

def fill_body(number_of_posts, top_posts, data_dict): # Fills the body of the post

    body_list = []
    body_     = []
    pic       = []
    today     = 'Last posted: ' + str(datetime.today()) + ' UTC' + '\n (updated every 15 minutes)'
    update    = ('\n'
                            + '----------------------------------------------' + '\n'
                            + h(4, c(today)))
    sign      = italic('Coded by @hyperion')
    nbsp      = '|' + blank_space(15) + '|' + blank_space(80) + '|'

    for i in range(0, len(data_dict)):
        p         = '|'
        url       = data_dict[i]['url']
        link1     = 'https://steemit.com' + url

        if p in remove_non_ascii(str(data_dict[i]['title'])):
                title = remove_non_ascii(str(data_dict[i]['title'])).split(p)
                title = title[0] + ': ' + title[1]

        else:
                title = remove_non_ascii(str(data_dict[i]['title']))

        date     = format_date(float(data_dict[i]['age']))
        post     = smart_format(title)
        author   = '_by @' + str(data_dict[i]['author']) + '_ '
        image    =  '[' + '!' + '[]' + '(' + data_dict[i]['image'] + ')' + ']' + '(' + link1 + ')'
        pic.append(image)
        r        = payout(i, number_of_posts, data_dict)
        Or       = str(round(r['organic_payout'], 2))
        OI       = str(round(r['relative_organic_payout'], 2))
        t_payout = str(round(r['total_payout'], 2))
        ot       = r['organic_trending']
        payout_l = '$' + Or + ' out of ' + '$' + t_payout  + ' (TOUSD ' + str(ot) + ')'
        post_au  = [post, author + italic(date), h(6, payout_l)]
        body     = (ot, p + pic[i] + p + make_list(post_au) + p + '\n')

        body_list.append(body)
        n_body   = sorted(body_list, reverse = True)


    for j in range(0, top_posts):
            body_.append(n_body[j][1])

    body_[0] = (body_[0] + '|---|---|' + '\n') # Removes empty header from the markdown table

    b     = concatenate(body_)

    table = b + nbsp + update + '\n' + h(7, c(sign))

    return table

def start(title = '', body = '', tags = [], timer = int): # Submits the post and initialises a timer

    submit_post(title, tags, body, 'steem.organic')
    args = [title, tags, body, timer]
    s    = Timer(timer, start, args)

    s.start()
    s_check = s.is_alive()

    if s_check == True: print('A timer of', timer, 'seconds was sucessfully initialised')
    else: print('Timer not working')
















        