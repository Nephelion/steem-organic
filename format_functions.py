from beem.discussions import Query, Discussions_by_author_before_date

from datetime import datetime
import time
							
							# FORMAT FUNCTIONS #

def concatenate(list):
    result = ''
    for element in list:
        result += str(element)
    return(result)

def c(string):
	c = '<center>' + string + '</center>'
	return(c)

def right(string):
	r = '<right>' + string + '</right>'
	return(r)

def l(string):
	l = '<left>' + string + '</left>'
	return(l)

def h(number, string):
	n  = str(number) + '>'
	h1 = '<h'
	h2 = '</h'
	h  = h1 + n + ' ' + string + ' ' + h2 + n

	return(h)

def italic(string):
	it = '_' + string + '_'

	return it

def make_list(items = []):
	ul1 = '<ul>'
	ul2 = '</ul>'
	l   = []

	for i in range(0, len(items)):
		ln = str(items[i])
		l.append(ln)

	new_l = concatenate(l)

	return(ul1 + new_l + ul2)

def smart_format(title):
	
	n = len(title)
	
	if n < 30: t = h(1, title)
	elif n >= 30 and n < 60: t = h(2, title)
	elif n >= 60 and n < 90: t = h(3, title)
	elif n >= 90 and n < 120: t = h(4, title)
	elif n >= 120 and n < 150: t = h(5, title)
	elif n >= 150 and n < 180: t = h(6, title)
	elif n >= 180 and n < 210: t = h(7, title)

	return(t)

def blank_space(n):
	blank_space = n * '&nbsp;'

	return(blank_space)

def remove_non_ascii(s): return("".join(i for i in s if ord(i) < 128))

def unix_time(string):

	format = '%Y-%m-%dT%H:%M:%S'
	ut     = time.mktime(datetime.strptime(string, format).timetuple())

	return ut

def str_to_time(string):
	format = '%Y-%m-%dT%H:%M:%S'
	ut     = datetime.strptime(string, format)
	return ut

def get_age(date):
	today = time.time() 
	age   = today - date

	return age

def format_date(date):
	
	if date < 60: date = ' just now.'
		
	elif date < 3600:
		date = int(date / 60)
		if date == 1: date = str(date) + ' minute ago.'
		elif date > 1: date = str(date) + ' minutes ago.'
	
	elif date < 86400: 
		date = int(date / 3600)
		if date == 1: date = str(date) + ' hour ago.'
		elif date > 1: date = str(date) + ' hours ago.'

	elif date >= 86400:
		date = int(date / 86400)
		if date == 1: date = str(date) + ' day ago.'
		elif date > 1: date = str(date) + ' days ago.'
		elif date > 7: date = ' more than a week ago.'

	return date


def parse_author_and_permlink():
	
	posts = str(Discussions_by_author_before_date(limit = 1, author = "steem.organic")[0])
	posts = posts.split(' ')
	posts = posts[1].split('>')
	posts = posts[0].split('/')

	author, permlink = posts[0], posts[1]

	return author, permlink






