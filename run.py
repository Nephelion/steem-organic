from steem_organic import get_data, total_rshares, get_upvote_value, payout, fill_body, start, s
from format_functions import unix_time, str_to_time
from threading import Timer
from datetime import datetime
from format_functions import remove_non_ascii, unix_time, parse_author_and_permlink
from threading import Timer

import time
		
		## Enter the following parameters ##

# top_posts: the number of posts you want to display (int)
# number_of_posts: the number of posts from which you want to fetch the data (1 - 300) (int)
# title: the title of the post (str)
# refresh: refresh rate (secs)
# repost: repost rate (secs)
# tags: the tags you want to add (list of strings)

def run_algo(top_posts, number_of_posts, title, refresh, repost, tags):
	
	''' 
	The title should not contain any date, because it will be automatically added every 'repost' seconds
	as a repost criterion 
	'''

	today      = datetime.today()
	now        = time.time()
	d          = parse_author_and_permlink()
	t_0        = unix_time(s.get_content(d[0], d[1])['created'])
	age_0      = round(now - t_0, 3)
	title_hour = ' ' + str(today.hour) + ':' + str(today.minute)
	title_date = ' ' + str(today.day) + '/' + str(today.month) + '/' + str(today.year)

	if age_0 > repost: 
		title = title + title_hour + title_date 
		print('A new post has been created')
	else: 
		pass

	data_dict = get_data(number_of_posts)
	table     = fill_body(number_of_posts, top_posts, data_dict)

	start(title, table, tags, refresh)

	args       = [top_posts, number_of_posts, title, refresh, repost, tags]
	algo_timer = Timer(repost, run_algo, args)

	algo_timer.start()
	timer_check = algo_timer.is_alive()

	if timer_check: pass
	else: print('Algo timer not working.')

run_algo(15, 75, 'Repost test for', 450, 900, ['spam', 'test'])


