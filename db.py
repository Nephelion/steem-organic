import sqlite3

# Setup
conn = sqlite3.connect('blacklist.db')
c    = conn.cursor()

# Functions
def create_table():
	c.execute("CREATE TABLE black_list (name text UNIQUE)")
	print('Table was created.')
	conn.commit()

def delete_table(name):
	c.execute("DROP TABLE", name)
	conn.commit()
	print(name, 'table was deleted')

def insert(name): # Inserts an account into blacklist
	c.execute("INSERT OR IGNORE INTO black_list VALUES(?)" , (name,))
	conn.commit()

def select_all(): # Outputs the list of all blacklisted accounts
	c.execute("SELECT * FROM black_list")
	return(c.fetchall())

def delete_all():
	c.execute("DELETE FROM black_list")
	conn.commit()
	print('Entries successfully deleted')

def delete(name):
	c.execute("DELETE FROM black_list WHERE name=:name", {'name' : name})
	conn.commit()
	print(name, ' was successfully deleted')

def is_in_blacklist(name): # Returns true if 'name' is in blacklist, or false if it's not
	c.execute("SELECT * FROM black_list WHERE name=:name", {'name' : name})
	data  = c.fetchall()
	if (name,) in data: print(name, 'is in blacklist')
	else: print(name, 'is not in blacklist')

def update(users_list):
	for i in range(0, len(users_list)):
		insert(users_list[i])



